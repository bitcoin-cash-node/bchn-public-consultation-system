Bitcoin Cash Node Public Consultation System - June 2020 Survey on CPFP
=======================================================================

This is a community survey by the Bitcoin Cash Node project, to gather feedback on who is using the Child-Pays-For-Parent feature in Bitcoin Cash (BCH) and what those use cases are.

See the bottom section for information on how to fill out and submit your feedback to us.


`%< --- BEGIN feedback section --- you can cut your reply here ---`


Question 1 - Have you used Child-Pays-For-Parent (CPFP) on BCH?
---------------------------------------------------------------

- [ ] - Yes

- [ ] - No


Question 2 - If you answered 'Yes' above, what were/are your use cases?
-----------------------------------------------------------------------

Describe your use cases for CPFP below.

1. Use case: ...

2. Use case: ...

3. Use case: ...


... (fill in and add or remove list items as needed)


Question 3 - Are you planning to use CPFP in the future?
--------------------------------------------------------

- [ ] - No plans

- [ ] - Yes, but do not have a specific timeframe

- [ ] - Yes, I'm planning to use it in the next ............... (give timeframe)


Information about yourself
--------------------------

This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive

- [ ] - a Bitcoin Cash pool operator or executive

- [ ] - a Bitcoin Cash exchange operator or executive

- [ ] - a Bitcoin Cash payment service provider operator or executive

- [ ] - a Bitcoin Cash protocol or full node client developer

- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)

- [ ] - an executive of a retail business using Bitcoin Cash

- [ ] - a person with significant amounts of their long term asset holdings in BCH

- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash

- [ ] - a BCH user

- [ ] - other - please specify: ....................................


`>% --- END feedback section --- you can cut your reply here ---`


How to fill out this survey
---------------------------

Above, you will find a feedback section with some questions.

The last section is to get an idea of what role(s) best describes how you see yourself in the Bitcoin Cash ecosystem. This information is optional, but it helps us better understand what certain groups of BCH users need.

We will not ask you for personally identifying information, and you can submit this survey form anonymously to us.


How to submit this survey
-------------------------

If you wish complete anonymity, use one of these ways:

- send to info@bitcoincashnode.org from a burner email
- save your response in an online text dump service (suggestions: https://hastebin.com or https://glot.io/new/plaintext) and convey the URL of your saved response to anyone in the BCHN project with instructions to forward it on to freetrader or the BCHN Representative, Tracy Chen.
- submit via Memo in topic https://memo.cash/topic/BCHN+PCS+input)
- raise an Issue on Gitlab (https://gitlab.com/bitcoin-cash-node/bchn-public-consultation-system/) with an anon account
- DM to freetrader in Bitcoin Cash Node slack (bitcoincashnode.slack.com) or via the telegram or freenode IRC bridges to BCHN slack).

See https://bitcoincashnode.org for ways to get in touch with the project. With the methods listed, you'd need to use a burner email or pseudonymous profile to submit your response.

If you don't mind associating your response with an already known identity:
- use any of the aforementioned submission channels with a known profile.
- You can submit it in the public channels #general-discuss or #support-general on Bitcoin Cash Node slack. We will pick up on it!


Submission deadline
-------------------

No fixed deadline on this survey, but your swift feedback will help us to plan our technical work.
