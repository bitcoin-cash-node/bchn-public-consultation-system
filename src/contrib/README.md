This folder is for source code contributed by others to BCHN PCS.

Only sources contributed under an OSI-compatible open source license
shall be accepted.

The contributed sources must indicate the license applicable to them.
